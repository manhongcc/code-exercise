#!/bin/bash
set -e
###part A
req_count=`grep -P '^([0-9]+\.){3}[0-9]+ [^ ]+ [^ ]+ \[[0-9]{2}/[A-Za-z]{3}/[0-9]{4}:[0-9]{2}:[0-9]{2}:[0-9]{2} [\+|\-][0-9]{4}\] ".*HTTP.*?" [0-9]{3}' sre_test_access.log | wc -l`
echo "- Count the total number of HTTP requests recorded by this access logfile: $req_count"
###part B
echo "- Find the top-10 (host) hosts makes most requests from 2017-06-10 00:00:00 to 2017-06-19 23:59:59, inclusively:"
grep -P '^([0-9]+\.){3}[0-9]+ [^ ]+ [^ ]+ \[19/Jun/2017:[0-9]{2}:[0-9]{2}:[0-9]{2} [\+|\-][0-9]{4}\] ".*HTTP.*?" [0-9]{3}' sre_test_access.log | grep -Po '^([0-9]+\.){3}[0-9]+' | sort | uniq -c | sort -nr | head -n 10 
###part C
echo "- Find out the country with most requests originating from (according the source IP):"
declare -A country_occurrence_map
declare -A ip_occurrence_map
repeatance=`grep -P '^([0-9]+\.){3}[0-9]+ [^ ]+ [^ ]+ \[[0-9]{2}/[A-Za-z]{3}/[0-9]{4}:[0-9]{2}:[0-9]{2}:[0-9]{2} [\+|\-][0-9]{4}\] ".*HTTP.*?" [0-9]{3}' sre_test_access.log | grep -Po '^([0-9]+\.){3}[0-9]+' | sort | uniq -c | sort -nr`
ip_repeatance=` echo "$repeatance" | grep -oP '([0-9]+\.){3}[0-9]+'`
count_line=`echo "$ip_repeatance" | wc -l`

batch_size=100

for (( i=1; i<=$count_line; i=$i+$batch_size ));
do
    batch=`echo "$ip_repeatance" | sed -n "${i},$((i+batch_size-1))p"`
    query_data=`jq -Rsn '
        [inputs
         | . / "\n"
         | (.[] | select((. | length) > 0) | . / "\t") as $input
         | {"query": $input[0], "fields": "country,query" }]
    ' <<< "$batch"`
    response=`curl -s ip-api.com/batch --data "$query_data"` # 150req/min
    echo -n '.'
    result=`echo $response | jq -r '.[] | "\(.query) \(.country)"'`
    while IFS=' ' read ipaddr country; do
        if [[ -z "$country" ]]; then
        	echo "empty returned:-$ipaddr-$country-"
        	country="empty_returned"
        fi
        sanitised_ipaddr=`echo $ipaddr | sed "s#\.#\\\.#g"`
        occurrence=`echo "$repeatance" | grep -P " $sanitised_ipaddr$" | grep -oP '([0-9]+ )' | sed "s/ //g"`
        current_occ=${country_occurrence_map["${country}"]}
        new_occ=`expr $current_occ + $occurrence` 
        country_occurrence_map["${country}"]=$new_occ
    done <<<"$result"
done
echo '***********stat***********'
max_country=""
max_req=-1
for key in "${!country_occurrence_map[@]}"; do
    echo -e "$key\t\t${country_occurrence_map[$key]}"
    if [[ ${country_occurrence_map[$key]} -gt $max_req ]]; then
        max_country=$key
        max_req=${country_occurrence_map[$key]}
    fi
done
echo '***********stat***********'
echo "Max $max_req number of requests originating from $max_country"
exit 0