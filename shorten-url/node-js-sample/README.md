# node-js-sample

A barebones Node.js app using [Express 4](http://expressjs.com/).
Copy from git@github.com:heroku/node-js-sample.git

## Running Locally

Make sure you have [Node.js](http://nodejs.org/) and the installed.

```sh
cd node-js-sample
npm install
npm start
```

Your app should now be running on [localhost:5000](http://localhost:5000/).

Build image
```sh
make build
make push
```