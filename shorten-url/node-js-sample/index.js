var express = require('express')
var bodyParser = require("body-parser")
var app = express()
var MongoClient = require('mongodb').MongoClient
var uuidBase62 = require('uuid-base62')
const uuidv4 = require('uuid/v4')
var hostname="shorturl.com"

//var mongourl = "mongodb://localhost:27017/shortenurl"
var mongourl = "mongodb://mongo-0.mongo,mongo-1.mongo,mongo-2.mongo:27017/shortenurl?replicaSet=rs0"


app.set('port', (process.env.PORT || 5000))
app.use(express.static(__dirname + '/public'))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

function parseUrl(raw) { 
  if (raw.indexOf('://') !== -1 ) {
    let abc = raw.split('://')
    console.log(abc[1])
    return abc[1]
  }
  return raw
}


app.get('/', function(req, res) {
  res.send('Hello World!')
})

app.get('/*', function(req, res) { 
  var b62 = req.originalUrl.replace('/','')

  MongoClient.connect(mongourl, function(err, db) {
    if(!err) {
      var doc1 = {'b62': b62}
      db.collection('links').findOne(
        doc1,
        {fields:{url:1}},
        function (error, response) {
          if(error) {
              console.log('Error occurred:', error)
          } else {
            if(response){
              console.log('Found:', response)
              res.redirect(301, "http://" + response.url)
            } else {
              res.send('http://' + hostname + '/' + b62 + ' not found!')
            }
          }
      })
    } else {
      console.log(err)
      res.json({
        err: 1,
        msg: err
      })
    }
  })

})

app.post('/submit',function(req,res){
  var raw_url = req.body.url
  var url = parseUrl(raw_url)

  MongoClient.connect(mongourl, function(err, db) {
    if(!err) {
      db.collection('links').findOne(
        {'url': url},
        function (error, response) {
          if(error) {
            console.log('Error occurred:', error)
          } else {
            if(!response) {
              var uuid = uuidv4()
              var b62 = uuidBase62.encode(uuid).substring(0, 8)
              db.collection('links').insertOne({'url': url, '_id': uuid, b62: b62});
            } else {
              var b62 = response.b62
            }
            console.log('Existing:', response)
            res.json({
              url: raw_url,
              shorten_url: "http://"+ hostname + "/" + b62
            })
          }
      })
    } else {
      console.log(err)
      res.json({
        err: 1,
        msg: err
      })
    }
  })
})

app.listen(app.get('port'), function() {
  console.log("Node app is running at localhost:" + app.get('port'))
})
