# 1. Log analytics
### Prerequisites
- `sudo yum install -y jq #OR sudo apt-get install -y jq`

### RUN
- extract sre_test_access.log.xz.gz
    ```
    chmod u+x log_parse.sh
    ./log_parse.sh
    ```
- When resolving the Country by IP, querying ip-api.com/batch in batch size of 100 IPs per request. And Max 150 requests per minute is limited by the API


# 2. SSH to AWS instances by Name tag

    chmod u+x goto.sh
    ./goto.sh ec2-name

- Unique Name tag assumed, or only the first match will be used
- Search for ec2 instance in default region
- Search other regions if no instance found in default region

# 3. Simple bit.ly shortening URL clone
- Prerequisites
    - A running K8S cluster(tested on 1.8 minikube) with statefulset, docker

- composed of mongodb & nodejs with redundency to minimize SPOF
    - mongodb replica set (initial 3 nodes replSet)
    - stateless node.js as api servers (init 2 pods)
- Scalability
    - nodejs can be scale up/down easily
    ` kubectl scale deployment node-shortenurl --replicas=N`
    - nodejs need to update the mongo URL(mongourl) when mongoDB is scaled up/down
    `kubectl scale statefulsets mongo --replicas=N`

- Deploy
    - As it directly pull mongo and shortenurl images from dockerhub. Simply apply the yaml files to deploy in K8S
    ```
    kubectl apply -f mongo.yaml
    kubectl apply -f nodejs.yaml
    ```

- Assumptions and limits
    - Each of shortened urls is presumed to be unqiue with very unlikely chance (1 in 62^8) of collision.
    - Input validation is by no means completed

- The application is tested on Minikube 1.8.0, you may encounter some problems in StorageClass and PresistenceVolume provisioning when running on Cloud Provider e.g. AWS. Change storeageClass in mongo.yaml to suit your needs.


- Data schema: **shortenurl.links**
    ```
    { "_id" : "789844fd-fcc8-483f-8434-134a67b8c79f", "url" : "amazon.com", "b62" : "3FyFkoan" }
    { "_id" : "c5fb0ae6-b7e4-4747-8fdd-2508f34afc03", "url" : "www.hko.gov.hk/contente.htm", "b62" : "61AdldzQ" }
    { "_id" : "260d64ce-33c3-43e3-b99b-f983f066942d", "url" : "hk.yahoo.com", "b62" : "19NPqcjS" }
    ```
- NodeJS main logic is in index.js
    -  Using Express.js handle request/response, then connect to mongDB to retrieve/store the shortened & original URL.


