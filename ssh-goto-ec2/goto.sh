#!/bin/bash
set -e
remote_user='ec2-user'
default_region=`aws configure get region`
if [[ -n $1 ]]; then
	ec2=`aws ec2 describe-instances --region $default_region --filter "Name=tag:Name,Values=$1" | jq -r '.Reservations[0].Instances[0].PublicIpAddress'`
	#Search other regions if default region not found
	if [[ $ec2 == "null" ]]; then
		echo "Not found in default region $default_region. Searching other regions"
		region_list=`aws ec2 describe-regions | jq -r --arg default_region "$default_region" '.Regions[] | select( (.RegionName != $default_region)) | .RegionName'`
	    while IFS='' read region; do
			ec2=`aws ec2 describe-instances --region $region --filter "Name=tag:Name,Values=$1" | jq -r '.Reservations[0].Instances[0].PublicIpAddress'`
			[ $ec2 != "null" ] && break
	    done <<<"$region_list"
	fi
fi

if [[ -z $1 || $ec2 == "null" ]]; then
	echo "Host not found"
else
	ssh $remote_user@$ec2
fi
